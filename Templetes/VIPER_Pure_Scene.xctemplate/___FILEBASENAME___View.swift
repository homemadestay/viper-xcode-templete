//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//

import Foundation
import UIKit

class ___VARIABLE_viewControllerName___: UIViewController {
    
    internal var presenter: ___VARIABLE_presenterName___Protocol?
    
    lazy var label: UILabel = {
        let element = UILabel()
        element.text = ""
        element.textAlignment = .left
        return element
    }()

    var viewModel: ___VARIABLE_viewModelName___? {
        presenter?.viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()
        initConstriantLayout()
    }
    
}

private extension ___VARIABLE_viewControllerName___ {
    func initViews() {
        self.view.backgroundColor = Colors.white
        
        self.view.addSubview(label)
    }
    
    func initConstriantLayout() {
        label.snp.makeConstraints({ make in

        })
    }
}

extension ___VARIABLE_viewControllerName___: ___VARIABLE_viewControllerName___Protocol {
    
}
