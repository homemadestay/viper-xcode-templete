//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//

import Foundation
import UIKit

class ___VARIABLE_presenterName___: ___VARIABLE_presenterName___Protocol {
   
    internal weak var view: ___VARIABLE_viewControllerName___Protocol?
    internal var interactor: ___VARIABLE_interactorName___InputProtocol?
    internal var wireFrame: ___VARIABLE_wireframeName___Protocol?
   
    var viewModel: ___VARIABLE_viewModelName___ = ___VARIABLE_viewModelName___()
   
    func viewDidLoad() {
       
    }
    
}

extension ___VARIABLE_presenterName___: ___VARIABLE_interactorName___OutputProtocol {
 
    
}
