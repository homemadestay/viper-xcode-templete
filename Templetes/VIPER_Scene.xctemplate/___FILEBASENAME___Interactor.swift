//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//

import Foundation
import UIKit

class ___VARIABLE_interactorName___: ___VARIABLE_interactorName___InputProtocol {
        
    internal var presenter: ___VARIABLE_interactorName___OutputProtocol?
    
}
