//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//

import Foundation
import UIKit

class ___VARIABLE_viewControllerName___: UIViewController {
    
    internal var presenter: ___VARIABLE_presenterName___Protocol?
    
    var viewModel: ___VARIABLE_viewModelName___? {
        presenter?.viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyStyle()
    }
    
    func applyStyle() {
       
    }
    
}

private extension ___VARIABLE_viewControllerName___ {
    
}

extension ___VARIABLE_viewControllerName___: ___VARIABLE_viewControllerName___Protocol {
    
}
