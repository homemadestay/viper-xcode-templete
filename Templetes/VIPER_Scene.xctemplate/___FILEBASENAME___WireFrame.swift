////
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//

import Foundation
import UIKit

class ___VARIABLE_wireframeName___: ___VARIABLE_wireframeName___Protocol {

    class func loadFromStoryborad() -> ___VARIABLE_viewControllerName___ {
        let storyboard = UIStoryboard(name: "___VARIABLE_storyboardName___",
                                      bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: ___VARIABLE_viewControllerName___.className) as! ___VARIABLE_viewControllerName___
    }
    
    class func createModule() -> ___VARIABLE_viewControllerName___ {
        let view: ___VARIABLE_viewControllerName___Protocol = ___VARIABLE_wireframeName___.loadFromStoryborad()
        let presenter: ___VARIABLE_presenterName___Protocol &
        ___VARIABLE_interactorName___OutputProtocol = ___VARIABLE_presenterName___()
        let interactor: ___VARIABLE_interactorName___InputProtocol = ___VARIABLE_interactorName___()
        let wireFrame: ___VARIABLE_wireframeName___Protocol = ___VARIABLE_wireframeName___()
        
        view.presenter = presenter
        presenter.view = view
        presenter.wireFrame = wireFrame
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view as! ___VARIABLE_viewControllerName___
    }
    
    // PRESENTER -> WIREFRAME
    
}

