//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//

import Foundation
import UIKit

protocol ___VARIABLE_viewControllerName___Protocol: AnyObject {

    var presenter: ___VARIABLE_presenterName___Protocol? { get set }
    
    // PRESENTER -> VIEW
   
}

protocol ___VARIABLE_wireframeName___Protocol: AnyObject {
    
    static func createModule() -> ___VARIABLE_viewControllerName___
    
    // PRESENTER -> WIREFRAME    
  
}

protocol ___VARIABLE_presenterName___Protocol: AnyObject {
    
    var view: ___VARIABLE_viewControllerName___Protocol? { get set }
    var interactor: ___VARIABLE_interactorName___InputProtocol? { get set }
    var wireFrame: ___VARIABLE_wireframeName___Protocol? { get set }
    
    var viewModel: ___VARIABLE_viewModelName___ { get }
    
    // VIEW -> PRESENTER
    func viewDidLoad()
    
}

protocol ___VARIABLE_interactorName___InputProtocol: AnyObject {
    
    var presenter: ___VARIABLE_interactorName___OutputProtocol? { get set }
    
    
    // PRESERNTER -> INTERACTOR
    
}

protocol ___VARIABLE_interactorName___OutputProtocol: AnyObject {
    
    // INTERACTOR -> PRESENTER
   
}
