//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//

import Foundation

protocol ___VARIABLE_serviceName___ServiceProtocol {
            
    static func fetch(request: ___VARIABLE_serviceModelName___.Fetch.Request,                           
                      handler: @escaping ((Result<Any, NSError>) -> Void))
}

enum ___VARIABLE_serviceName___Service: ServiceRouterProtocol {
    
    case fetch(request: ___VARIABLE_serviceModelName___.Fetch.Request)
    
    var params: [String : String] {
        switch self {
        case .fetch(let request):
            return [ : ]
      
        default:
            return [ : ]
            
        }
    }
    
    var service: APIService {
        switch self {
        case .fetch:
            return .init(withServiceName : "fetch_something",
                         withPath : "/path",
                         withMethod: .get)
        
        }
    }
    
}
