//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//

import Foundation
import SwiftyJSON

class ___VARIABLE_serviceName___RemoteService: ___VARIABLE_serviceName___ServiceProtocol {
    
    static func fetch(request: ___VARIABLE_serviceModelName___.Fetch.Request,
                      handler: @escaping ((Result<Any, NSError>) -> Void)) {
        let service = HotelService.fetch(request: request).service
        HMSAPI.request(apiService: service,
                       onSuccess: { (response) in
            guard
                let value = response.value
            else {
                handler(.failure(.create("Invalid data")))
                return
            }
            
            let json = JSON(value)
            
            guard
                let model = Model(json)
            else {
                handler(.failure(.create("Invalid data")))
                return
            }
            
            handler(.success(model))
            
           }) { (error) in
               handler(.failure(error))
           }
    }
        
}
