
# DRY - don't repeat yourself

![xExample](documents/x-code-template.png)
# HMS xCode templates
## What is xcode template
With Xcode we create files and groups everyday. We commonly create files for our classes, storyboards or XIBs. We organize them into folders in order to have a logically organized project. Our preferred IDE offers a number of useful built in templates that we can use to create different types of projects or files.

----

## Component Libraries
- Scene - will create
    - Storyboard.storyboard
    - View.swift
    - Interactor.swift
    - Presenter.swift
    - Worker.swift
    - Wireframe.swift
    - ViewModel.swift

----

## Installation xCode template
```
make install_templates
```

## uninstall
```
make uninstall_templates 
```
